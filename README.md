# README

---

## Web Geeks Dojo

This project is an example of converting a custom site from Bootstrap v3 to Bootstrap v4. It is also a base project to be used in our WebRTC project.

### How do I get set up?
* run `npm install`
* run `gulp`

* Making edits to SASS can be automatically compiled by running `gulp watch`